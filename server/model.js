const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

//change db to correct one!!
mongoose.connect('mongodb+srv://eddy:password1234@cluster0-odlqq.mongodb.net/Truckyourself?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

var jobSchema = new mongoose.Schema({
    puadd: {
        type: String,
        required: true
    },
    pucity: {
        type: String,
        required: true
    },
    pustate: {
        type: String,
        required: true
    },
    puzip: {
        type: String,
        required: true
    },
    pudescr: {
        type: String,
        required: true
    },
    destadd: {
        type: String,
        required: true
    },
    destcity: {
        type: String,
        required: true
    },
    deststate: {
        type: String,
        required: true
    },
    destzip: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    },
    jobPending: {
        type: Boolean,
        default: true,
        required: true
    },
    jobPostedAt: {
        type: Date,
        default: new Date()
    },
    Available: {
        type: Boolean,
        required: true
    },
});

var userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    encryptedPassword: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    usertype: {
        type: String,
        required: true
    }
});

userSchema.methods.setEncryptedPassword = function (plainPassword, callbackFunction) {
    //this is a user instance
    bcrypt.hash(plainPassword, 12).then(hash => {
        //still the user instance because of the arrow
        this.encryptedPassword = hash;
        callbackFunction();
    });
};

userSchema.methods.verifyPassword = function (plainPassword, callbackFunction) {
    bcrypt.compare(plainPassword, this.encryptedPassword).then(result => {
        callbackFunction(result);
    });
};

var User = mongoose.model('User', userSchema);
var Job = mongoose.model('Job', jobSchema);

module.exports = {
    Job: Job,
    User: User
};