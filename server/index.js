const express = require('express');
const cors = require('cors');
const session = require('express-session');
const passport = require('passport');
const passportLocal = require('passport-local');



const model = require('./model');
var ObjectID = require('mongodb').ObjectID;
const app = express();
const port = process.env.PORT || 3000;
//Global to keep track of user types as they login to be routed
let usrtype;
//MIDDLEWARE
app.use(express.json()); //Used to parse JSON bodies
app.use(cors({ credentials: true, origin: 'http://66.42.84.69' }));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

//secret is a sign for cookies
app.use(session({ secret: ';dlfjg-kuy39tiq]-kas[poigj[o', resave: false, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());

//PASSPORT CONFIG
passport.use(new passportLocal.Strategy({
}, function (username, password, done) {
    //authentication logic success or fail
    //async find user in DB by username
    model.User.findOne({ username: username }).then(function (user) {
        if (!user) {
            //user not found return failure
            return done(null, false);
        } else {
            //user exists async compare given password to password in DB
            user.verifyPassword(password, function (result) {
                if (result) {
                    //success
                    usrtype = user.usertype;
                    userId = user._id
                    return done(null, user);
                } else {
                    //failure
                    return done(null, false);
                }
            });
        }
    }).catch(function (err) {
        //unlikely but database err
        done(err);
    });
}));

// PASSPORT SERIALIZATION AND DESERILIZATION
passport.serializeUser(function (user, done) {
    //called on succesful authentication
    done(null, user._id);
});

passport.deserializeUser(function (userID, done) {
    //called before future requests following succesful authentication
    model.User.findOne({ _id: ObjectID(userID) }).then(function (user) {
        done(null, user);
    }).catch(function (err) {
        done(err);
    });
});

//SESSION
app.delete('/session', function (req, res) {
    req.logOut();
    res.sendStatus(200);
});
app.get('/session', function (req, res) {
    if (req.user) {
        res.json(req.user);

    } else {
        res.sendStatus(401);
    }
});
// RESTful authenticate
app.post('/session', passport.authenticate('local'), function (req, res) {
    res.send({
        usrtype: usrtype,
        userId: userId
    });
});

//USERS
app.get('/users', function (req, res) {
    model.User.find().then(function (user) {
        res.json(user);
    });
});

app.post('/jobs', function (req, res) {
    let job = new model.Job({
        puadd: req.body.puadd,
        pucity: req.body.pucity,
        pustate: req.body.pustate,
        puzip: req.body.puzip,
        pudescr: req.body.pudescr,
        destadd: req.body.destadd,
        destcity: req.body.destcity,
        deststate: req.body.deststate,
        destzip: req.body.destzip,
        userId: req.body.userId,
        jobPending: req.body.jobPending,
        Available: true,
    });
    job.save().then(function () {
        res.sendStatus(201);
    }).catch(function (err) {
        if (err.errors) {
            var messages = {};
            for (let e in err.errors) {
                messages[e] = err.errors[e].message;
            }
            res.status(422).json(messages);
        } else {
            res.sendStatus(500);
        }
    });
});


app.get('/jobs', function (req, res) {
    if (req.headers['userid']) {
        model.Job.find({ userId: req.headers['userid'] }).sort('jobPostedAt').then(function (jobs) {
            res.send(jobs)
        }).catch((err) => {
            console.log(err)
            res.send('error')
        })
    }
    else {
        model.Job.find().then(function (jobs) {
            res.send(jobs)
        }).catch((err) => {
            console.log(err)
            res.send('error')
        })
    }

});

app.get('/jobs/:id', function (req, res) {
    model.Job.findById(req.params.id).then(function (job) {
        res.send(job)
    }).catch((err) => {
        console.log(err)
        res.send('error')
    })
});
app.delete('/jobs/:id', async (req, res) => {
    try {
        await model.Job.findByIdAndRemove(req.params.id);
        res.status(200).send("job Deleted")
    }
    catch (err) {
        console.log(err);
        res.status(404)
    }
});
app.put('/completejob/:id', async (req, res) => {
    // update jobs for completion
    const updatedJob = {};
    updatedJob.jobPending = false;
    try {
        job = await model.Job.findByIdAndUpdate(
            req.params.id,
            { $set: updatedJob },
            { new: true }
        );
        res.json(job);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
});
app.put('/Acceptjob/:id', async (req, res) => {
    //reserve job so only one person can accept
    const updatedJob = {};
    updatedJob.Available = false;
    try {
        job = await model.Job.findByIdAndUpdate(
            req.params.id,
            { $set: updatedJob },
            { new: true }
        );
        res.json(job);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
});
app.put('/jobs/:id', async (req, res) => {

    // recreating job to update
    const updatedJob = {};
    if (req.body.puadd) updatedJob.puadd = req.body.puadd;
    if (req.body.pucity) updatedJob.pucity = req.body.pucity;
    if (req.body.pustate) updatedJob.pustate = req.body.pustate;
    if (req.body.puzip) updatedJob.puzip = req.body.puzip;
    if (req.body.pudescr) updatedJob.pudescr = req.body.pudescr;
    if (req.body.destadd) updatedJob.destadd = req.body.destadd;
    if (req.body.destcity) updatedJob.destcity = req.body.destcity;
    if (req.body.deststate) updatedJob.deststate = req.body.deststate;
    if (req.body.destzip) updatedJob.destzip = req.body.destzip;
    try {
        job = await model.Job.findByIdAndUpdate(
            req.params.id,
            { $set: updatedJob },
            { new: true }
        );
        res.json(job);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
});
app.post('/users', function (req, res) {
    let user = new model.User({
        username: req.body.username,
        usertype: req.body.usertype,
        email: req.body.email
    });
    user.setEncryptedPassword(req.body.password, function () {
        //insert into mongoose model
        user.save().then(function () {
            res.status(201).send(user._id);
        }).catch(function (err) {
            if (err.errors) {
                var messages = {};
                for (let e in err.errors) {
                    messages[e] = err.errors[e].message;
                }
                res.status(422).json(messages);
            } else {
                //duplicate emails 
                res.sendStatus(500);
            }
        });
    });
});
app.listen(port, () => {
    console.log(`Truckyourself listening at http://66.42.84.69:${port}`);
});