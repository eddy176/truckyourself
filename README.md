# Truck Yourself

Truck yourself allows users to signup so that they can post items to be moved for them when they can not do it themselves or they can sign up to move items for other users. User passwords are hashed using bcrypt upon signup and login. When users login they are authenticated using a local passport strategy. Finally Google maps API is used to give directions to users when jobs are posted or accepted.

![Truckee](images/trkee.png)


