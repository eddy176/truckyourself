import { createRouter, createWebHashHistory } from 'vue-router'
import Signup from '../views/Signup.vue'
import Login from '../views/Login.vue'
import Trucker from '../views/Trucker.vue'
import Truckee from '../views/Truckee.vue'


const routes = [
  {
    path: '/',
    name: 'Loview',

    children: [
      { path: '', component: Login },
      { path: '/login', component: Login },
      { path: '/signup', component: Signup }
    ],
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "loview" */ '../views/Loview.vue')
  },
  {
    path: '/trucker',
    name: 'Trucker',
    component: Trucker
    /*children: [
      { path: '', component: Content }
    ]*/
  },
  {
    path: '/truckee',
    name: 'Truckee',
    component: Truckee
    /*children: [
      { path: '', component: Content }
    ]*/
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
